#include "threadpool.h"
#include <stdlib.h>
#include "list.h"
/**
 * threadpool.c
 *
 * A work-stealing, fork-join thread pool.
 */

//Typedef to improve readability
typedef struct list list;
typedef struct future future;
typedef struct thread_pool pool;
typedef struct list_elem list_elem;
static pool* tPool;
/*
 *
 * This struct is used to maintain the thread pool
 * */
struct thread_pool
{
	//Add fields spec
	pthread_mutex_t threadLock;
	pthread_cond_t cond;
	pthread_t* threadID;
	list tasksGlobal;
	list* tasksLocal;
	int nThreads;
	
};
struct future
{
	void* returnValue;
	void* args;
	fork_join_task_t task;
	list_elem elem;
	sem_t semaphore;
};


/* Create a new thread pool with no more than n threads. */
struct thread_pool * thread_pool_new(int nthreads)
{
	//Initialize struct
	pool *threadPool = malloc(sizeof(pool))
	//Initialize the mutex variable
	pthread_mutex_init(threadPool->threadLock,NULL); 
        //Initialize the condition variable
	pthread_cond_init(threadPool->cond, NULL);
        //Initiliaze the dynamic array of threadIDs
	pool->threadID = malloc(nthreads*(sizeof(pthread_t)));
        //Initialize the task list
	list_init(threadPool->tasksGlobal);
	//Set the number of threads in the struct
        threadPool->nThreads = nthreads;
	

	//Initialize function pointer for thread
	void (*threadFuncPointer)(int);
	threadFuncPointer = threadFunc;
	//Create threads
	for(int i=0;i<nthreads;i++)
	{
		if(pthread_create(&(threadPool->threadID[i]), NULL, threadFuncPointer, NULL))
		{
			 perror("ERROR creating thread");
		}
		else
		{
			list_init(threadPool->tasksLocal[i]);
		}
	}
	tPool = threadPool;	
	return threadPool;
}
/*
 *
 *A function that is called by all the threads
 *
 */
void* threadFunc(void* args)
{
	return NULL;
}
/* 
 * Shutdown this thread pool in an orderly fashion.  
 * Tasks that have been submitted but not executed may or
 * may not be executed.
 *
 * Deallocate the thread pool object before returning. 
 */
void thread_pool_shutdown_and_destroy(struct thread_pool * threadPool)
{
	
	
	for(int i=0;i<threadPool->Nthreads;i++)
        {
        	pthread_join(threadPool->threadID[i], NULL);
        }
	free(threadPool->threadID);
	free(threadPool);	
}

/* 
 * Submit a fork join task to the thread pool and return a
 * future.  The returned future can be used in future_get()
 * to obtain the result.
 * 'pool' - the pool to which to submit
 * 'task' - the task to be submitted.
 * 'data' - data to be passed to the task's function
 *
 * Returns a future representing this computation.
 */
struct future * thread_pool_submit(struct thread_pool *pool, fork_join_task_t task, void * data)
{
	future* fut = malloc(sizeof(fut));
	fut->task = task;
	fut->args = data;
	
	sem_init(&fut->sem, 0, 0);
	pthread_mutex_lock(&pool->threadLock);
	list_push_back(&pool->tasksGlobal, &fut->elem);
	pthread_cond_signal(&pool->cond);
	pthread_mutex_unlock(&pool->threadLock)
	return fut;
		
}

/* Make sure that the thread pool has completed the execution
 * of the fork join task this future represents.
 *
 * Returns the value returned by this task.
 */
void * future_get(future * fut)
{
	sem_wait(&(fut->semaphore);
	return fut->returnValue;
}

/* Deallocate this future.  Must be called after future_get() */
void future_free(future * fut)
{
	free(fut);
}
