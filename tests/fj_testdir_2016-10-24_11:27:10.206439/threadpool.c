#include "threadpool.h"
#include <stdlib.h>
#include <stdio.h>
#include "list.h"
#include <semaphore.h>
#include <pthread.h>
#include <stdbool.h>

/**
 * threadpool.c
 *
 * A work-stealing, fork-join thread pool.
 */

//Typedef to improve readability
typedef struct list list;
typedef struct future future;
typedef struct thread_pool pool;
typedef struct list_elem list_elem;
static pool* tPool;
/*
 *
 * This struct is used to maintain the thread pool
 * */
struct thread_pool
{
	//Add fields spec
	pthread_mutex_t threadLock;
	pthread_cond_t cond;
	pthread_t* threadID;
	list tasksGlobal;
	list* tasksLocal;
	int nThreads;
	bool shutdown;
	
};
struct future
{
	bool globalTask;
	int thread;
	void* returnValue;
	void* args;
	bool running;
	fork_join_task_t task;
	list_elem elem;
	sem_t semaphore;
};
/*
 *
 *  A function that is called by all the threads
 *   
 *    
 */
static void* threadFunc(void* args)
{
	while(!tPool->shutdown)
	{
		pthread_mutex_lock(&tPool->threadLock);
		if(tPool->shutdown)
                {
       	                        pthread_mutex_unlock(&tPool->threadLock);
                                pthread_exit(NULL);
                }
		while(list_empty(&tPool->tasksGlobal))
		{
			pthread_cond_wait(&tPool->cond,&tPool->threadLock);
			if(tPool->shutdown)
			{
				pthread_mutex_unlock(&tPool->threadLock);
				pthread_exit(NULL);
			}
		}
		printf("jishnu");
		list_elem* e = list_pop_front(&tPool->tasksGlobal);
		future* fut = list_entry(e,future,elem);
		pthread_cond_signal(&tPool->cond);
		pthread_mutex_unlock(&tPool->threadLock);
		void* retVal  = (*fut->task)(tPool,fut->args);
		fut->returnValue = retVal;	
		sem_post(&fut->semaphore);
	}
	pthread_exit(NULL);
	return NULL;
}
/* Create a new thread pool with no more than n threads. */
struct thread_pool * thread_pool_new(int nthreads)
{
	//Initialize struct
	pool *threadPool = malloc(sizeof(pool));
	//Initialize the mutex variable
	pthread_mutex_init(&threadPool->threadLock,NULL); 
        //Initialize the condition variable
	pthread_cond_init(&threadPool->cond, NULL);
        //Initiliaze the dynamic array of threadIDs
	threadPool->threadID = malloc(nthreads*(sizeof(pthread_t)));
        //Initialize the task list
	list_init(&threadPool->tasksGlobal);
	//Set the number of threads in the struct
        threadPool->nThreads = nthreads;
	
	threadPool->shutdown = false;
	//Initialize function pointer for thread
	//Create threads
	for(int i=0;i<nthreads;i++)
	{
		if(pthread_create(&(threadPool->threadID[i]), NULL, (void*)&threadFunc, NULL))
		{
			 perror("ERROR creating thread");
		}
		else
		{
			list_init(&threadPool->tasksLocal[i]);
		}
	}
	tPool = threadPool;	
	return threadPool;
}
/*
static int getThreadNum()
{
	for(int i=0;i<tPool->nThreads;i++)
	{
		if(tPool->threadID[i] == pthread_self())
		{
			return i;
		}
	}
	return -1;
}*/

/* 
 * Shutdown this thread pool in an orderly fashion.  
 * Tasks that have been submitted but not executed may or
 * may not be executed.
 *
 * Deallocate the thread pool object before returning. 
 */
void thread_pool_shutdown_and_destroy(struct thread_pool * threadPool)
{
	pthread_mutex_lock(&threadPool->threadLock);
	threadPool->shutdown = true;
	pthread_cond_broadcast(&threadPool->cond);
	pthread_mutex_unlock(&threadPool->threadLock);
	
	for(int i=0;i<threadPool->nThreads;i++)
        {
        	pthread_join(threadPool->threadID[i], NULL);
        }
	pthread_mutex_destroy(&threadPool->threadLock);
	pthread_cond_destroy(&threadPool->cond);
	free(threadPool->threadID);
	free(threadPool);	
}

/* 
 * Submit a fork join task to the thread pool and return a
 * future.  The returned future can be used in future_get()
 * to obtain the result.
 * 'pool' - the pool to which to submit
 * 'task' - the task to be submitted.
 * 'data' - data to be passed to the task's function
 *
 * Returns a future representing this computation.
 */
struct future * thread_pool_submit(struct thread_pool *pool, fork_join_task_t task, void * data)
{
	future* fut = malloc(sizeof(future));
	fut->task = task;
	fut->args = data;
	
	sem_init(&fut->semaphore, 0, 0);
	pthread_mutex_lock(&pool->threadLock);
	list_push_back(&pool->tasksGlobal, &fut->elem);
	pthread_cond_signal(&pool->cond);
	pthread_mutex_unlock(&pool->threadLock);
	return fut;
		
}

/* Make sure that the thread pool has completed the execution
 * of the fork join task this future represents.
 *
 * Returns the value returned by this task.
 */
void * future_get(future * fut)
{
	//if(!fut->running)
	//{
	//	pthread_mutex_lock(&tPool->threadLock);
	//	list_push_back(&(tPool->tasksLocal[getThreadNum()]),list_remove(&fut->elem));
	//	pthread_cond_signal(&(tPool->cond));
	//	pthread_mutex_unlock(&(tPool->threadLock));	
	//}
	sem_wait(&(fut->semaphore));
	return fut->returnValue;
}

/* Deallocate this future.  Must be called after future_get() */
void future_free(future * fut)
{
	free(fut);
}
