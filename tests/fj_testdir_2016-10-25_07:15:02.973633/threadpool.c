#include "threadpool.h"
#include <stdlib.h>
#include <stdio.h>
#include "list.h"
#include <semaphore.h>
#include <pthread.h>
#include <stdbool.h>

/**
 * threadpool.c
 *
 * A work-stealing, fork-join thread pool.
 */

//Typedef to improve readability
typedef struct list list;
typedef struct future future;
typedef struct thread_pool pool;
typedef struct list_elem list_elem;
typedef struct worker worker;
/*
 *
 * This struct is used to maintain the thread pool
 * */
struct thread_pool
{
	//Add fields spec
	pthread_mutex_t threadLock;
	pthread_cond_t cond;
	list tasksGlobal;
	list wList;
	int nThreads;
	bool shutdown;
	
};
struct future
{
	pool* threadPool;
	int stat;
	//0 - queued
	//1 - Running
	//2 - finished
	void* returnValue;
	void* args;
	fork_join_task_t task;
	list_elem elem;
	sem_t semaphore;
};
struct worker
{
	pthread_t ID;
	list tasks;
	list_elem elem;
};
/*
 *
 *  A function that is called by all the threads
 *   
 *    
 */
static bool __thread isInternal;
static worker* workerThread;
static list_elem* stealTask(pool* tPool)
{
        list_elem * e;
        worker *w = NULL;
        for(e = list_begin(&tPool->wList); e != list_end(&tPool->wList); e = list_next(e))
        {
                w = list_entry(e, worker, elem);
                if (!list_empty(&w->tasks))
                {
                        return list_pop_back(&w->tasks);
                }
        }
        return NULL;
}
static list_elem* dequeueGlobal(pool* tPool)
{
        if(!list_empty(&tPool->tasksGlobal))
        {
                return list_pop_front(&tPool->tasksGlobal);
        }
        return NULL;
}
static list_elem* dequeueLocal(worker* wThread)
{
        if(!list_empty(&wThread->tasks))
        {
                return list_pop_front(&wThread->tasks);
        }
        return NULL;
}

static void* threadFunc(void* args)
{
	
	pool* tPool = (pool*)args;
	pthread_mutex_lock(&tPool->threadLock);
	//Set the thread local variables
	isInternal = true;
	
	list_elem * e;
        worker *w = NULL;
        for(e = list_begin(&tPool->wList); e != list_end(&tPool->wList); e = list_next(e))
        {
                w = list_entry(e, worker, elem);
        	if(pthread_self() == w->ID)
		{
			workerThread =w; 
		}
	}
	
	while(!tPool->shutdown)
	{
		list_elem *taskElem;
		if((taskElem=dequeueLocal(w))==NULL)
		{
			if((taskElem=dequeueGlobal(tPool))==NULL)
			{
				if((taskElem=stealTask(tPool))==NULL)
				{
					tPool->shutdown = true;
				}
			}
		}

		future* fut = list_entry(taskElem,future,elem);
		fut->stat = 1;
		void* retVal  = (*fut->task)(tPool,fut->args);
		fut->returnValue = retVal;	
		fut->stat = 2;
		sem_post(&fut->semaphore);
	}
	pthread_mutex_unlock(&tPool->threadLock);
	pthread_exit(NULL);
	return NULL;
}
/* Create a new thread pool with no more than n threads. */
struct thread_pool * thread_pool_new(int nthreads)
{
	
	//Initialize struct
	pool *threadPool = malloc(sizeof(pool));
	//Initialize the mutex variable
	pthread_mutex_init(&threadPool->threadLock,NULL); 
        //Initialize the condition variable
	pthread_cond_init(&threadPool->cond, NULL);
        //Initialize the task list
	list_init(&threadPool->tasksGlobal);
	//Set the number of threads in the struct
        threadPool->nThreads = nthreads;
	threadPool->shutdown = false;
	list_init(&threadPool->wList);
	
	//Create threads
	for(int i=0;i<nthreads;i++)
	{
		struct worker* w;
		w = malloc(sizeof(worker));
		list_push_front(&threadPool->wList, &w->elem);
		list_init(&w->tasks);
		if(pthread_create(&(w->ID), NULL, (void*)&threadFunc, threadPool))
		{
			 perror("ERROR creating thread");
		}
	}
	return threadPool;
}

/* 
 * Shutdown this thread pool in an orderly fashion.  
 * Tasks that have been submitted but not executed may or
 * may not be executed.
 *
 * Deallocate the thread pool object before returning. 
 */
void thread_pool_shutdown_and_destroy(struct thread_pool * threadPool)
{
	pthread_mutex_lock(&threadPool->threadLock);
	threadPool->shutdown = true;
	pthread_cond_broadcast(&threadPool->cond);
	pthread_mutex_unlock(&threadPool->threadLock);
	
	//List traversal
	list_elem * e;
	worker *w;
	for(e = list_begin(&threadPool->wList); e != list_end(&threadPool->wList); e = list_next(e))
        {
		w = list_entry(e, worker, elem);
        	pthread_join(w->ID, NULL);
        }
	//Deallocating memory
	while(!list_empty(&threadPool->wList))
	{
		free(list_entry(list_pop_front(&threadPool->wList),worker, elem));
	}
	pthread_mutex_destroy(&threadPool->threadLock);
	pthread_cond_destroy(&threadPool->cond);
	free(threadPool);	
}

/* 
 * Submit a fork join task to the thread pool and return a
 * future.  The returned future can be used in future_get()
 * to obtain the result.
 * 'pool' - the pool to which to submit
 * 'task' - the task to be submitted.
 * 'data' - data to be passed to the task's function
 *
 * Returns a future representing this computation.
 */
struct future * thread_pool_submit(struct thread_pool *pool, fork_join_task_t task, void * data)
{
	future* fut = malloc(sizeof(future));
	fut->task = task;
	fut->args = data;
	fut->stat = 0;
	fut->threadPool = pool;	
	sem_init(&fut->semaphore, 0, 0);
	pthread_mutex_lock(&pool->threadLock);
	if(isInternal)
	{
		list_push_front(&workerThread->tasks, &fut->elem);	
	}
	else
	{
		list_push_back(&pool->tasksGlobal, &fut->elem);
	}
        pthread_cond_signal(&pool->cond);
        pthread_mutex_unlock(&pool->threadLock);
	return fut;
		
}

/* Make sure that the thread pool has completed the execution
 * of the fork join task this future represents.
 *
 * Returns the value returned by this task.
 */
void * future_get(future * fut)
{
	pthread_mutex_lock(&fut->threadPool->threadLock);
	if(fut->stat==0)
	{
		list_remove(&fut->elem);
		pthread_mutex_unlock(&fut->threadPool->threadLock);
		fut->stat = 1;
		fut->returnValue = (*fut->task)(fut->threadPool,fut->args);
	}
	else
	{
		sem_wait(&fut->semaphore);
	}
	return fut->returnValue;
}

/* Deallocate this future.  Must be called after future_get() */
void future_free(future * fut)
{
	free(fut);
}
